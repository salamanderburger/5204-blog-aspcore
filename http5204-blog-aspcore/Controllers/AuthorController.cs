﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_blog_aspcore.Models;
using http5204_blog_aspcore.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace http5204_FirstMVC.Controllers
{
    public class AuthorController : Controller
    {
        //makes a BlogCMSContext
        private readonly BlogCMSContext db;
        private readonly IHostingEnvironment _env;
        //constructor function which takes a BlogCMSContext as a constructor.
        //Q: How does the Controller just *get* this context?
        //A: "magic" called dependency injection will put the data there.

        //We need the usermanager class to get things like the id or name
        //Right now we only have one user type (ApplicationUser) but we could have others.
        private readonly UserManager<ApplicationUser> _userManager;
        //This function will return the current user at some point when called
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AuthorController(BlogCMSContext context,IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            //When our author controller is created, we need three things
            //1: The database context (for interacting with the database)
            db = context;
            //2: The hosting environment (for getting filepaths for author img)
            _env = env;
            //3: The usermanager (for getting the current logged in user)
            //Q: How did you figure this out?
            //A: https://stackoverflow.com/questions/30701006/how-to-get-the-current-logged-in-user-id-in-asp-net-core
            //A2: Look at the code for AccountController.cs
            _userManager = usermanager;
        }
        /*
           Reading links to fully understand this file
           -------------------------------------------
            Image uploading with ASP Core
            https://docs.microsoft.com/en-us/aspnet/core/mvc/models/file-uploads?view=aspnetcore-2.2

            I don't really understand how Model, View, Controller works
            https://www.tomdalling.com/blog/software-design/model-view-controller-explained/
            https://medium.freecodecamp.org/model-view-controller-mvc-explained-through-ordering-drinks-at-the-bar-efcba6255053
            
            EF Core + ASP Core tutorial
            https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/intro?view=aspnetcore-2.2

            EF Core + ASP Core tutorial 2
            https://www.learnentityframeworkcore.com/walkthroughs/aspnetcore-application

            How can I get Entity Framework Core?
            https://www.learnentityframeworkcore.com/efcore/how-to-get

            How can I use the fancy Entity Framework Core commands? (add, edit, delete)
            https://www.learnentityframeworkcore.com/dbset

            How can I do more advanced commands? (join, where, find)
            https://www.learnentityframeworkcore.com/dbset/querying-data

            I just want to write my own query, How can I do that?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            How can I know more about parameterized queries?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            What is this "db" variable?
            https://www.learnentityframeworkcore.com/dbcontext
        */
        //This function asks important questions from the user
        //It was built for a need to understand more information about a particular user
        //given a context.
        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is an author or not.
            //UserState
            //0 => No User
            //1 => has user has no author
            //2 => has user has author but no blogs
            if (user == null) return 0;
            var userid = user.Id;
            if (user.AuthorID == null) return 1; //User has no author
            else
            {
                return 2; //user has author
                
            }
            
        }


        // GET: Authors
        public async Task<ActionResult> Index()
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["UserAuthorID"] = 0; //default to 0
            if (userstate==2)
            {
                ViewData["UserAuthorID"] = user.AuthorID;
                
            }
            return View(await db.Authors.ToListAsync());
        }

        public ActionResult Show(int id)
        {
            //wrapper function. Show will redirect to details.
            return RedirectToAction("Details/"+id);
        }

        // GET: Authors/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            //get the current user id for now (proof of concept).
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserAuthorID"] = 0;
            if (userstate == 2) ViewData["UserAuthorID"] = user.AuthorID;

            Author located_author = await db.Authors.Include(a => a.Blogs).SingleOrDefaultAsync(a => a.AuthorID == id);
            if (located_author == null)
            {
                return NotFound();
            }
            //Check the current user's author against the author.
            //Debug.WriteLine("Asked for user in details. The user is " + user.Id.ToString());
            //Check what the located author id is
            //Debug.WriteLine("Asked for located author in details. The located author is" + located_author.AuthorID);
            //This one only works when we pick meg as the author
            //Debug.WriteLine("Asked for user's author in details. The user is " + user.author.ToString());
            //Debug.WriteLine("Asked for user's phone number in details. The phone number is");

            ViewData["UserState"] = userstate;
            return View(located_author);
        }

        // GET: Authors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Authors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("AuthorID,AuthorFName,AuthorLName,AuthorPenName")] Author author)
        {
            
            if (ModelState.IsValid)
            {
                db.Authors.Add(author);
                db.SaveChanges();
                //In addition to creating the author, we want to assign the current logged in user to the current author
                //However, the ability to pull user info is "asynchronous"
                //See how GetCurrentUser is async method type and returns await keyword
                //We make a new async method which maps the currentuser's author to this author
                var res = await MapUserToAuthor(author); //make account the author
                
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
            
            
       
        }
        
        // GET: Authors/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            Author author = db.Authors.Find(id);
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 2)
            {
                if (id != user.AuthorID) return Forbid();
                return View(author);
            }
            return NotFound();
        }

        // POST: Authors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("AuthorID,AuthorFName,AuthorLName,AuthorPenName")] Author author, IFormFile authorimg)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 2)
            {
                if (author.AuthorID != user.AuthorID) return Forbid();
                //If we are here that means we can edit this author
                //default to false (no pic)
                author.HasPic = 0;
                var webRoot = _env.WebRootPath;
                if (authorimg != null)
                {
                    if (authorimg.Length > 0)
                    {
                        //file extensioncheck taken from https://www.c-sharpcorner.com/article/file-upload-extension-validation-in-asp-net-mvc-and-javascript/
                        var valtypes = new[] { "jpeg", "jpg", "png", "gif" };
                        var extension = Path.GetExtension(authorimg.FileName).Substring(1);

                        if (valtypes.Contains(extension))
                        {

                            //generic .img extension, web translates easily.
                            string fn = author.AuthorID + "." + extension;

                            //get a direct file path to imgs/authors/
                            string path = Path.Combine(webRoot, "images/authors");
                            path = Path.Combine(path, fn);

                            //save the file
                            using (var stream = new FileStream(path, FileMode.Create))
                            {
                                authorimg.CopyTo(stream);
                            }
                            //let the model know that there is a picture with an extension
                            author.HasPic = 1;
                            author.ImgType = extension;

                        }
                    }
                }
                if (ModelState.IsValid)
                {
                    db.Entry(author).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(author);
            }
            //Something went wrong.
            return NotFound();
        }

        // GET: Authors/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var author = db.Authors.FindAsync(id);
            if (author == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 2)
            {
                if (id != user.AuthorID) return Forbid();
                return View(author);
            }
            return NotFound();
        }

        // POST: Authors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Author author = await db.Authors.FindAsync(id);
            var user = await GetCurrentUserAsync();
            if (user.AuthorID != id)
            {
                return Forbid();
            }
            await UnmapUserFromAuthor(id);
            db.Authors.Remove(author);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        //This function was created to unlink the 1-1 relationship between users and authors
        //The reason being if we delete an author we don't want to violate foreign key constraints on the users table
        //The only tricky part is doing it with asynchronous calls.
        public async Task<IActionResult> UnmapUserFromAuthor(int id)
        {
            //stripping user from author
            Author author = await db.Authors.FindAsync(id);
            author.user = null;
            author.UserID = "";
            if (ModelState.IsValid)
            {
                db.Entry(author).State = EntityState.Modified;
                var author_res = await db.SaveChangesAsync();
                if (author_res == 0)//No changes detected
                {
                    return BadRequest(author_res);
                }
                else
                {
                    //unmap user from author succsessful
                    //Now to strip author from user
                    var user = await GetCurrentUserAsync();
                    user.author = null;
                    user.AuthorID = null;
                    var user_res = await _userManager.UpdateAsync(user);
                    if (user_res == IdentityResult.Success)
                    {
                        Debug.WriteLine("I was able to update the user");
                        return Ok();
                    }
                    else
                    {
                        Debug.WriteLine("I was not able to update the user");
                        return BadRequest(user_res);
                    }
                }
            }
            else
            {
                return BadRequest("Unstable Model.");
            }
            
        }

        //This function is supposed to update the current user and map the author
        //which was just created.
        //To map a user, we have to set one to one relationship on both tables
        private async Task<IActionResult> MapUserToAuthor(Author author)
        {
            var user = await GetCurrentUserAsync();
            //mapping the user to the author
            user.author = author;
            var user_res = await _userManager.UpdateAsync(user);
            //checking confirmation of mapping user to author
            if (user_res == IdentityResult.Success)
            {
                Debug.WriteLine("I'm told we mapped the author to the user.");
            }
            else
            {
                Debug.WriteLine("We were not able to map the author to the user.");
                return BadRequest(user_res);
            }
            //mapping the author to the user
            author.user = user;
            author.UserID = user.Id;
            //checking that 
            if (ModelState.IsValid)
            {
                db.Entry(author).State = EntityState.Modified;
                var author_res = await db.SaveChangesAsync();
                if (author_res > 0) //some authors affected
                {
                    //getting here means we mapped the user to the author
                    //and we also mapped the author to the user
                    return Ok();
                }
                else
                {
                    return BadRequest(author_res);
                }
            }
            else
            {
                return BadRequest("Unstable Author Model.");
            }

        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}
