﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_blog_aspcore.Models;
using http5204_blog_aspcore.Models.ViewModels;
using http5204_blog_aspcore.Data;
using System.Diagnostics;

namespace http5204_blog_aspcore.Controllers
{
    public class BlogController : Controller
    {
        //makes a BlogCMSContext
        private readonly BlogCMSContext db;
        //constructor function which takes a BlogCMSContext as a constructor.
        //Q: How does the Controller just *get* this context?
        //A: "magic" called dependency injection will put the data there.
        
        //We need the usermanager class to get things like the id or name
        //Right now we only have one user type (ApplicationUser) but we could have others.
        private readonly UserManager<ApplicationUser> _userManager;
        //This function will return the current user at some point when called
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);


        public BlogController(BlogCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        /*
           Reading links to fully understand this file
           -------------------------------------------
            I don't really understand how Model, View, Controller works
            https://www.tomdalling.com/blog/software-design/model-view-controller-explained/
            https://medium.freecodecamp.org/model-view-controller-mvc-explained-through-ordering-drinks-at-the-bar-efcba6255053
            
            EF Core + ASP Core tutorial
            https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/intro?view=aspnetcore-2.2

            EF Core + ASP Core tutorial 2
            https://www.learnentityframeworkcore.com/walkthroughs/aspnetcore-application

            How can I get Entity Framework Core?
            https://www.learnentityframeworkcore.com/efcore/how-to-get

            How can I use the fancy Entity Framework Core commands? (add, edit, delete)
            https://www.learnentityframeworkcore.com/dbset

            How can I do more advanced commands? (join, where, find)
            https://www.learnentityframeworkcore.com/dbset/querying-data

            I just want to write my own query, How can I do that?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            How can I know more about parameterized queries?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            What is this "db" variable?
            https://www.learnentityframeworkcore.com/dbcontext


        */
        
        //This function asks important questions from the user
        //It was built for a need to understand more information about a particular user
        //given a context.
        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is an author or not.
            //UserState
            //0 => No User
            //1 => has user has no author
            //2 => has user has author but no blogs
            //3 => has user has author and at least 1 blog
            if (user == null) return 0;
            var userid = user.Id;
            if (user.AuthorID == null) return 1; //User has no author
            else
            {
                var uauthorid = user.AuthorID;
                var userblogs = await db.Blogs.Where(b => b.AuthorID == uauthorid).ToListAsync();
                var userblogcount = userblogs.Count();
                if (userblogcount == 0) return 2;//User has no blogs
                else if (userblogcount > 0) return 3;
            }
            return -1;//something went wrong
        }

        public async Task<bool> IsUserBlogOwner(ApplicationUser user, int BlogID)
        {
            if (user == null) return false;
            if (user.AuthorID == null) return false;
            //purpose of this function is to check whether the user is the author of the blog
            var userblogs = await
                db
                .Blogs
                .Include(b => b.Author)
                .Where(b => b.AuthorID == user.AuthorID)
                .Where(b => b.BlogID == BlogID)
                .ToListAsync();
            if (userblogs.Count() > 0) return true;
            return false;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            //tell give us context about the user
            ViewData["UserState"] = userstate;
            ViewData["UserAuthorID"] = 0; //default 0
            if (userstate == 3)
            {
                ViewData["UserAuthorID"] = (int)user.AuthorID;
            }

            /*BLOG PAGINATION ALGORITHM*/
            var _blogs = await db.Blogs.Include(b => b.Author).ToListAsync();
            int blogcount = _blogs.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)blogcount/perpage)-1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage+1).ToString();
            }
            //DATA NEEDED: All Blogs in DB
            //However, we also have to include the info for the author on each blog
            //[List<Blog> blogs]=> variable named blogs which is a list of Blog
            //[.Include(b=>b.Author)]=> Get the blog's associated author
            //[.ToListAsync()]=>Return a list of information asynchronously

            //[.Skip(int)]=>ignore these many records
            //[.Take(int)]=>fetch only this many more
            List<Blog> blogs = await db.Blogs.Include(b=>b.Author).Skip(start).Take(perpage).ToListAsync();
            /*END OF BLLOG PAGINATION ALGORITHM*/
            return View(blogs);

            
            
        }

        public async Task<ActionResult> New()
        {
            //DATA NEEDED
            //we need to know the context of the user
            //are they registered as an author?
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            //userstate gets the context of the user
            switch (userstate)
            {
                case 0: return RedirectToAction("Register","Account");
                case 1: return RedirectToAction("Create","Author");
            }
            //The user should have an author if not 0 or 1
            ViewData["UserAuthorID"] = user.AuthorID;
            //Model defined in Models/ViewModels/BlogEdit.cs
            BlogEdit blogeditview = new BlogEdit();

            //GOTO Views/Blog/New.cshtml
            return View(blogeditview);
        }

        [HttpPost]
        public async Task<ActionResult> Create(string BlogTitle_New, int BlogAuthor_New, string BlogBio_New)
        {
            //need to check if the author is indeed who they say they are.
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            //Can't make a blog if you aren't an author or don't have an account
            if (userstate < 2) return Forbid();
            if (user.AuthorID != BlogAuthor_New)
            {
                //someone tried to impersonate a blog user by switching the hidden field ID
                return Forbid();
            }

            //Raw Query   
            string query = "insert into blogs (BlogTitle, BlogBio, AuthorID) values (@title, @bio, @author)";

            //SQL parameterized query technique
            //[SqlParameter[] myparams] => SET variable myparams as an array of type (SqlParameter)
            //[new SqlParameter[1]] => Create a new SqlParameter array with 3 items
            SqlParameter[] myparams = new SqlParameter[3];
            //@title paramter
            myparams[0] = new SqlParameter("@title", BlogTitle_New);
            //@bio parameter
            myparams[1] = new SqlParameter("@bio", BlogBio_New);
            //@author (id) FOREIGN KEY paramter
            myparams[2] = new SqlParameter("@author", BlogAuthor_New);
            
            //Run the parameterized query (DML - Data Manipulation Language)
            //Insert into blogs ( .. ) values ( .. ) 
            db.Database.ExecuteSqlCommand(query, myparams);

            //GOTO: 
            return RedirectToAction("List");
        }

        public async Task<ActionResult> Edit(int id)
        {
            //Data Needed:
            //One particular blog (we have the id)
            //That blogs' author
            //The user's author

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            bool isValid = await IsUserBlogOwner(user, id);
            if (!isValid) return Forbid();


            var myblog = db.Blogs.Find(id); //finds that blog
            if (myblog != null) return View(myblog);
            else return NotFound();   
        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id,string BlogTitle, int BlogAuthor, string BlogBio)
        {
            //[(id == null)] => No ID passed to GET (ie. Blog/Edit) instead of (Blog/Edit/3)
            //[||]=> OR logical operator
            //[(db.Blogs.Find(id) == null)] => No blog found in DB
            if (db.Blogs.Find(id) == null)
            {
                return NotFound();

            }

            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3)
            {
                bool isValid = await IsUserBlogOwner(user, id);
                if (!isValid) return Forbid();
                //something went right!

                //Raw Update MSSQL query
                string query = "update blogs set BlogTitle=@title, BlogBio=@bio, AuthorID=@author where blogid=@id";

                //[SqlParameter[] myparams] => SET variable myparams as an array of type (SqlParameter)
                //[new SqlParameter[1]] => Create a new SqlParameter array with 4 items
                SqlParameter[] myparams = new SqlParameter[4];
                //Parameter for @title "blogtitle"
                myparams[0] = new SqlParameter("@title", BlogTitle);
                //Parameter for @bio "blogbio"
                myparams[1] = new SqlParameter("@bio", BlogBio);
                //Parameter for (author) id FOREIGN KEY
                myparams[2] = new SqlParameter("@author", BlogAuthor);
                //Pararameter for (blog) id PRIMARY KEY
                myparams[3] = new SqlParameter("@id", id);

                //Execute the custom SQL command with parameters
                db.Database.ExecuteSqlCommand(query, myparams);

                //GOTO: View/Blog/Show.cshtml with paramter Blogid passed
                return RedirectToAction("Show/" + id);
            }
            //something went wrong
            return Forbid();
           
        }

        public async Task<ActionResult> Show(int id)
        {
            //[(id == null)] => No ID passed to GET (ie. Blog/Edit) instead of (Blog/Edit/3)
            //[||]=> OR logical operator
            //[(db.Blogs.Find(id) == null)] => No blog found in DB
            if (db.Blogs.Find(id)==null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            ViewData["UserState"] = userstate;
            ViewData["IsUserBlogOwner"] = false;
            if (userstate == 3)
            {
                ViewData["IsUserBlogOwner"] = await IsUserBlogOwner(user, id);
            }

            //Raw MSSQL query
            //string query = "select * from blogs where blogid=@id";
            //[SqlParameter[] myparams] => SET variable myparams as an array of type (SqlParameter)
            //[new SqlParameter[1]] => Create a new SqlParameter array with 1 item
            //SqlParameter myparam = new SqlParameter("@id", id);

            //Colloquial
            //Hey view.. here's a list of all the blogs in my db
            //include the pages I need
            //include the author (so I can see who wrote it)
            
            //[db.Blogs]=> My database blogs reference
            //[Include(b=>b.Pages)]=> Also include data for pages which belong to this blog
            //[SingleOrDefault(b=>b.blogID==id)]=> Get the blog where the id matches the input id.
            Blog myblog = await db.Blogs.Include(b=>b.Author).Include(b=>b.Pages).SingleOrDefaultAsync(b=>b.BlogID==id);

            return View(myblog);
            
        }
        
        public async Task<ActionResult> Delete(int id)
        {
            //[(id == null)] => No ID passed to GET (ie. Blog/Edit) instead of (Blog/Edit/3)
            //[||]=> OR logical operator
            //[(db.Blogs.Find(id) == null)] => No blog found in DB
            if (db.Blogs.Find(id) == null)
            {
                return NotFound();

            }
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            if (userstate == 3) {
                bool isValid = await IsUserBlogOwner(user, id);
                if (!isValid) return Forbid();
                //If it's not wrong it must be right!
                //These three statements should definitely be wrapped in
                //a stored procedure instead of me manually doing it
                //key term here "referential integrity"

                //delete pagesxtags associated with pages associated with blogs
                string query = "delete from PagesxTags where PageID in (select PageID from pages where BlogID=@id)";
                SqlParameter param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, param);

                //then delete associated pages
                query = "delete from pages where BlogID=@id";
                param = new SqlParameter("@id", id);
                db.Database.ExecuteSqlCommand(query, param);

                //finally delete blog
                query = "delete from Blogs where blogid=@id";
                param = new SqlParameter("@id", id);
                await db.Database.ExecuteSqlCommandAsync(query, param);
                return RedirectToAction("List");
            }
            //Something went wrong...
            return Forbid();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}