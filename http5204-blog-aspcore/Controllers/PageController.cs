﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_blog_aspcore.Models;
using http5204_blog_aspcore.Models.ViewModels;
using http5204_blog_aspcore.Data;
using System.Diagnostics;

namespace http5204_blog_aspcore.Controllers
{
    public class PageController : Controller
    {
        //makes a BlogCMSContext
        private readonly BlogCMSContext db;
        //constructor function which takes a BlogCMSContext as a constructor.
        //Q: How does the Controller just *get* this context?
        //A: "magic" called dependency injection will put the data there.

        //We need the usermanager class to get things like the id or name
        //Right now we only have one user type (ApplicationUser) but we could have others.
        private readonly UserManager<ApplicationUser> _userManager;
        //This function will return the current user at some point when called
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);


        public PageController(BlogCMSContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        /*
           Reading links to fully understand this file
           -------------------------------------------
            I don't really understand how Model, View, Controller works
            https://www.tomdalling.com/blog/software-design/model-view-controller-explained/
            https://medium.freecodecamp.org/model-view-controller-mvc-explained-through-ordering-drinks-at-the-bar-efcba6255053
            
            EF Core + ASP Core tutorial
            https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/intro?view=aspnetcore-2.2

            EF Core + ASP Core tutorial 2
            https://www.learnentityframeworkcore.com/walkthroughs/aspnetcore-application

            How can I get Entity Framework Core?
            https://www.learnentityframeworkcore.com/efcore/how-to-get

            How can I use the fancy Entity Framework Core commands? (add, edit, delete)
            https://www.learnentityframeworkcore.com/dbset

            How can I do more advanced commands? (join, where, find)
            https://www.learnentityframeworkcore.com/dbset/querying-data

            I just want to write my own query, How can I do that?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            How can I know more about parameterized queries?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            What is this "db" variable?
            https://www.learnentityframeworkcore.com/dbcontext


        */

        //This function asks important questions from the user
        //It was built for a need to understand more information about a particular user
        //given a context.
        public async Task<int> GetUserDetails(ApplicationUser user)
        {
            //check if the user is an author or not.
            //UserState
            //0 => No User
            //1 => has user has no author
            //2 => has user has author but no blogs
            //3 => has user has author and at least 1 blog
            if (user == null) return 0;
            var userid = user.Id;
            if (user.AuthorID == null) return 1; //User has no author
            else
            {
                var uauthorid = user.AuthorID;
                var userblogs = await db.Blogs.Where(b => b.AuthorID == uauthorid).ToListAsync();
                var userblogcount = userblogs.Count();
                if (userblogcount == 0) return 2;//User has no blogs
                else if (userblogcount > 0) return 3;
            }
            return -1;//something went wrong
        }

        public async Task<bool> IsUserPageOwner(ApplicationUser user, int PageID)
        {
            if (user == null) return false;
            if (user.AuthorID == null) return false;
            //purpose of this function is to check whether the user is the author of the page
            var userpages = await
                db
                .Pages
                .Include(p=>p.Blog)
                .ThenInclude(b=>b.Author)
                .Where(p=>p.PageID==PageID)
                .Where(p=>p.Blog.AuthorID==user.AuthorID)
                .ToListAsync();
            if (userpages.Count() > 0) return true;
            return false;
        }

        public async Task<bool> IsUserBlogOwner(ApplicationUser user, int BlogID)
        {
            if (user == null) return false;
            if (user.AuthorID == null) return false;
            //purpose of this function is to check whether the user is the author of the blog
            var userblogs = await
                db
                .Blogs
                .Include(b => b.Author)
                .Where(b => b.AuthorID == user.AuthorID)
                .Where(b => b.BlogID == BlogID)
                .ToListAsync();
            if (userblogs.Count() > 0) return true;
            return false;
        }

        // GET: Page
        public async Task <ActionResult> Index()
        {
            //Redirect to the list view method
            return RedirectToAction("List");
        }

        public async Task<ActionResult> New()
        {
            var user = await GetCurrentUserAsync();
            var state = await GetUserDetails(user);

            switch (state)
            {
                //0=>User has no account
                case 0: { return RedirectToAction("Register", "Account"); }
                //1=>User has account but no author
                case 1: { return RedirectToAction("Create", "Author"); }
                //2=>User has account and author but no blogs
                case 2: { return RedirectToAction("New", "Blog"); }
            }
            //case 3: => User has account and author and at least one blog
            return View(await db.Blogs.Where(b=>b.AuthorID==user.AuthorID).ToListAsync());
        }

        //Restricts this method to only handle POST
        //eg. POST to /Pages/Create/
        [HttpPost]
        public async Task<ActionResult> Create(string PageTitle_New, string PageContent_New, int PageBlog_New)
        {
            //You can only create a page if you are an author and you have a blog which matches
            var user = await GetCurrentUserAsync();
            var state = await GetUserDetails(user);

            switch (state)
            {
                case 0: { return RedirectToAction("Register", "Account"); }
                case 1: { return RedirectToAction("Create", "Author"); }
                case 2: { return RedirectToAction("New", "Blog"); }
                case 3:
                {
                    //This one is interesting. This means that
                    //user has author
                    //author has blogs.
                    //we need to know if the blog that is assigned is the correct one.
                    bool isValid = await IsUserBlogOwner(user, PageBlog_New);
                    var userblogs = await
                        db
                        .Blogs
                        .Where(b => b.AuthorID == user.AuthorID) //blogs belonging to that user
                        .Where(b => b.BlogID == PageBlog_New) //blogs matching the requested blogid
                        .ToListAsync();
                        
                    if (!isValid) return Forbid();

                    string query = "insert into Pages (PageTitle, PageContent, BlogID) " +
                    "values (@title,@content,@bid)";

                    SqlParameter[] myparams = new SqlParameter[3];
                    myparams[0] = new SqlParameter("@title", PageTitle_New);
                    myparams[1] = new SqlParameter("@content", PageContent_New);
                    myparams[2] = new SqlParameter("@bid", PageBlog_New);

                    db.Database.ExecuteSqlCommand(query, myparams);
                    //testing that the paramters do indeed pass to the method
                    //Debug>Windows>Output
                    //Debug.WriteLine(query);
                    return RedirectToAction("List");
                }
                default: return NotFound();
                    
            }

            
        }

        public async Task<ActionResult> Show(int id)
        {
            //raw query equivalent
            //string query = "select * from pages where pageid =@pageid";
            //Debug>Windows>Output
            //Debug.WriteLine(query);

            var user = await GetCurrentUserAsync();
            var state = await GetUserDetails(user);
            bool isValid = await IsUserPageOwner(user, id);
            ViewData["IsUserPageOwner"] = isValid;
           
            ViewData["UserState"] = state;
            //This view data will help us decide what to do when presenting the page to the user.

            //DATA NEEDED: page with it's tags and also the author
            //NOTE: FromSql is buggy, I don't know why.
            //[db.Pages.FromSql(query, new SqlParameter("@id",id))]=>Execute raw SQL (above)
            //[.Include(p=>p.Blog.Author)]=> Include the Pages Blog so that we can get the Blogs Author
            //[.Include(p=>p.pagesxtags)]=> Include the tag id bridging data
            //[.ThenInclude(pxt => pxt.Tag)] => Include actual tag information after we have id bridging data
            //[.SingleOrDefault(p=>p.PageID==id))]=> Get the one where the pageid is the id 
            return View(await db.Pages.Include(p=>p.Blog.Author).Include(p => p.pagesxtags).ThenInclude(pxt => pxt.Tag).SingleOrDefaultAsync(p=>p.PageID==id));
        }

        //This shows the edit interface
        //The edit interface is more advanced with a relational record now.
        public async Task<ActionResult> Edit(int id)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);
            Debug.WriteLine("the user state for user " + user.Id + " is " + userstate);
            //userstate==3 means the user has author and at least one blog.
            if (userstate == 3)
            {
                //find out if this user is even the page owner
                bool isValid = await IsUserPageOwner(user, id);
                Debug.WriteLine("The user is the page owner: "+isValid);
                if (!isValid) return Forbid();
            }
            else
            {
                //Many invalid situations... too complex to map them all
                return NotFound();
            }

            //DATA NEEDED: page data (with specific blog and tags), all blog data, all tag data
            //Modern Technique
            //Make a new compound view which holds the data we need 
            //to display page edit
            PageEdit pageedit = new PageEdit();
            //[db.Pages]=> get page data.
            //[.Include(p=>p.pagesxtags)]=> include (tags) data for page.
            //[.SingleOrDefault(p => p.PageID == id)]=> include the pages' blog. 
            // and Take the one where the ID matches GET parameter
            pageedit.Page = 
                db.Pages
                    .Include(p=>p.pagesxtags)
                    .Include(p=>p.Blog)
                    .SingleOrDefault(p => p.PageID == id);
            //get blogs that belong to this user
            pageedit.Blogs = db.Blogs.Where(b=>b.AuthorID==user.AuthorID).ToList();
            //get all tag data
            pageedit.Tags = db.Tags.ToList();
            //if we have info, pass it to Page/View.cshtml
            if (pageedit.Page != null) return View(pageedit);
            else return NotFound();
        }
        //This one actually does the editing commmand
        [HttpPost]
        public async Task<ActionResult> Edit(int id, string PageTitle, string PageContent, int? PageBlog, int?[] PageTags, int?[] Removed_PageTags)
        {
            var user = await GetCurrentUserAsync();
            var userstate = await GetUserDetails(user);

            if (userstate == 3)
            {
                bool isValid = await IsUserPageOwner(user, id);
                if (!isValid) return Forbid();
            }
            else
            {
                return NotFound();
            }

            //[(id == null) ]=>no ID in GET
            //[(db.Pages.Find(id) == null)]=>couldn't find this page
            if (db.Pages.Find(id) == null)
            {
                //Show error message
                return NotFound();

            }
            //Raw query data
            string query = "update pages set pagetitle=@title, " +
                "pagecontent=@content, " +
                "BlogID=@bid where pageid = @id";

            //parameter for @title
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter();
            myparams[0].ParameterName = "@title";
            myparams[0].Value = PageTitle;

            //parameter for @content
            myparams[1] = new SqlParameter();
            myparams[1].ParameterName = "@content";
            myparams[1].Value = PageContent;

            //parameter for @big (blogid) FOREIGN KEY
            myparams[2] = new SqlParameter();
            myparams[2].ParameterName = "@bid";
            myparams[2].Value = PageBlog;

            //parameter for @id (pageid) PRIMARY KEY
            myparams[3] = new SqlParameter();
            myparams[3].ParameterName = "@id";
            myparams[3].Value = id;         
            db.Database.ExecuteSqlCommand(query, myparams);

            //[(PageTags!=null)]=> No page tags found (GET parameter, result of checkboxes)
            //[(PageTags.Count() > 0)]=> No pages found in DB 
            if ((PageTags!=null) && (PageTags.Count() > 0))
            {
                //Go through each tag that the user wanted to add
                foreach (int tagid in PageTags)
                {
                    //If this page tag doesn't exist we skip
                    if (db.Tags.Find(tagid) == null) continue;

                    //I cannot enter a PagexTag record if it already exists.
                    if (db.Pages.Include(p => p.pagesxtags).SingleOrDefault(p => p.PageID == id).pagesxtags.Exists(pxt => pxt.Tagid == tagid)) continue;
                    
                    //also note I use tagquery and mytagparams as variable names
                    //so they don't collide with the ones above
                    string tagquery = "insert into PagesxTags (TagID,PageID) values (@tid,@pid)";
                    
                    Debug.WriteLine("I'm trying to insert a record " +
                        "into tagpages with tagid of " + tagid+
                        "and pageid of "+id);
                    
                    SqlParameter[] mytagparams = new SqlParameter[2];
                    mytagparams[0] = new SqlParameter("@tid", tagid);
                    mytagparams[1] = new SqlParameter("@pid", id);
                    db.Database.ExecuteSqlCommand(tagquery, mytagparams);
                }
            }
            //This code is the antonym of the code above
            //These checkboxes are reverse populated onchange in JS (see Page/Edit.cshtml)
            if ((Removed_PageTags != null) && (Removed_PageTags.Count() > 0))
            {
                //Go through each tag that is to be removed
                foreach (int tagid in Removed_PageTags)
                {
                    //This is a very similar codeblock as before but the exact opposite
                    //we are removing any page tags in this list.

                    //If this page tag doesn't exist we skip
                    if (db.Tags.Find(tagid) == null) continue;

                    //If a page has no tags, skip (none to remove)
                    if (db.Pages.Include(p=>p.pagesxtags).SingleOrDefault(p=>p.PageID==id).pagesxtags.Count()==0) continue;
                    
                    //is the page NOT already associated with this tag? if so, skip
                    if (!db.Pages.Include(p=>p.pagesxtags).SingleOrDefault(p=>p.PageID==id).pagesxtags.Exists(pxt => pxt.Tagid == tagid)) continue;

                    string tagremovequery = "delete from PagesxTags where Tagid=@tid and PageID=@pid";
                    //Debug>Windows>Output
                    /*
                    Debug.WriteLine("I'm trying to remove a record " +
                        "from tagpages with tagid of " + tagid +
                        "and pageid of " + id);
                    */
                    SqlParameter[] mytagrmparams = new SqlParameter[2];
                    //tag id parameter
                    mytagrmparams[0] = new SqlParameter("@tid", tagid);
                    //page id parameter
                    mytagrmparams[1] = new SqlParameter("@pid", id);
                    db.Database.ExecuteSqlCommand(tagremovequery, mytagrmparams);

                }
            }
            //GOTO: SHOW method in PageController.cs and pass argument (page)id
            return RedirectToAction("Show/" + id);
        }

        //GET of localhost/pages/delete/2 implies
        //delete action on pages controller with id 2
        public ActionResult Delete(int id)
        {
            //Establish referential integrity first
            string query = "delete from PagesxTags where Pageid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            //Then delete main record
            query = "delete from pages where pageid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id",id));

            //GOTO: method List in PageController.cs
            return RedirectToAction("List");
        }

        public async Task<ActionResult> List(int pagenum)
        {

            //Data Needed: All Pages and their tags and their author
            //Data Needed: Need person logged in as well.
            //ViewData["UserState"] => arbitrary array to store information
            //GetUserDetails => function created to pass an integer representing information about the user
            //[db.Pages]=>all pages in DB
            //[.Include(p=>p.Blog.Author)] => take that pages authors (display authorname)
            //[.Include(p=>p.pagesxtags)] => get bridging data (which tagid)
            //[.ThenInclude(pxt=>pxt.Tag)] = >get the actual tag data (like color, name)
            //[.ToList()]=>Return this info as a list

            //get the current user as well as the user state (see GetUserDetails)
            var user = await GetCurrentUserAsync();
            ViewData["UserState"] = await GetUserDetails(user);

            /*Pagination Algorithm*/
            var _pages = await db.Pages.Include(p => p.Blog.Author).Include(p => p.pagesxtags).ThenInclude(pxt => pxt.Tag).ToListAsync();
            int pagecount = _pages.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Page> pages = await db.Pages.Include(p => p.Blog.Author).Include(p => p.pagesxtags).ThenInclude(pxt => pxt.Tag).ToListAsync();
            /*End of Pagination  Algorithm*/

            return View(pages);
            
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}