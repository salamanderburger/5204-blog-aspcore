﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_blog_aspcore.Models;
using http5204_blog_aspcore.Data;

namespace http5204_blog_aspcore.Controllers
{
    public class SearchController : Controller
    {

        //makes a BlogCMSContext
        private readonly BlogCMSContext db;
        //constructor function which takes a BlogCMSContext as a constructor.
        //Q: How does the Controller just *get* this context?
        //A: "magic" called dependency injection will put the data there.
        public SearchController(BlogCMSContext context)
        {
            db = context;
        }
        public ActionResult Explore()
        {

            return View();
        }
        /*
           Reading links to fully understand this file
           -------------------------------------------
            How did you query into a "SearchItem.cs" type when it's not mapped to the DB?
            https://www.learnentityframeworkcore.com/query-types

            I don't really understand how Model, View, Controller works
            https://www.tomdalling.com/blog/software-design/model-view-controller-explained/
            https://medium.freecodecamp.org/model-view-controller-mvc-explained-through-ordering-drinks-at-the-bar-efcba6255053
            
            EF Core + ASP Core tutorial
            https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/intro?view=aspnetcore-2.2

            EF Core + ASP Core tutorial 2
            https://www.learnentityframeworkcore.com/walkthroughs/aspnetcore-application

            How can I get Entity Framework Core?
            https://www.learnentityframeworkcore.com/efcore/how-to-get

            How can I use the fancy Entity Framework Core commands? (add, edit, delete)
            https://www.learnentityframeworkcore.com/dbset

            How can I do more advanced commands? (join, where, find)
            https://www.learnentityframeworkcore.com/dbset/querying-data

            I just want to write my own query, How can I do that?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            How can I know more about parameterized queries?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            What is this "db" variable?
            https://www.learnentityframeworkcore.com/dbcontext

            
        */


        [HttpPost]
        public ActionResult Results(string searchkey)
        {
            //SearchItem is defined in Model/SearchItem.cs
            //SearchItems is referenced in Models/BlogCMSContext.cs
            //SearchItems is of type <DbQuery>, as of EF Core 2.1 can be used to map to non-entity types
            //What does that mean? It means I can build an arbitrary class (searchitem) and map results to it
            //The last piece of the puzzles is the mapping between column names and class fields
            //check the DATABASE VIEWS for a view of the same name as the DbQuery variable (SearchItems)


            var rawdata =
                db.SearchItems
                .Where(si =>
                    EF.Functions.Like(si.SearchTitle, "%" + searchkey + "%")
                    ||EF.Functions.Like("%"+searchkey+"%", si.SearchTitle)
                    || EF.Functions.Like(si.SearchContent, "%" + searchkey + "%")
                    || EF.Functions.Like("%" + searchkey + "%", si.SearchContent))
                .ToList();



            return View(rawdata);
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}