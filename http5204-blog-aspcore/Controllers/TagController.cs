﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using http5204_blog_aspcore.Models;
using http5204_blog_aspcore.Data;

namespace http5204_blog_aspcore.Controllers
{
    public class TagController : Controller
    {
        //makes a BlogCMSContext
        private readonly BlogCMSContext db;
        //constructor function which takes a BlogCMSContext as a constructor.
        //Q: How does the Controller just *get* this context?
        //A: "magic" called dependency injection will put the data there.
        public TagController(BlogCMSContext context)
        {
            db = context;
        }
        /*
           Reading links to fully understand this file
           -------------------------------------------
            I don't really understand how Model, View, Controller works
            https://www.tomdalling.com/blog/software-design/model-view-controller-explained/
            https://medium.freecodecamp.org/model-view-controller-mvc-explained-through-ordering-drinks-at-the-bar-efcba6255053
            
            EF Core + ASP Core tutorial
            https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/intro?view=aspnetcore-2.2

            EF Core + ASP Core tutorial 2
            https://www.learnentityframeworkcore.com/walkthroughs/aspnetcore-application

            How can I get Entity Framework Core?
            https://www.learnentityframeworkcore.com/efcore/how-to-get

            How can I use the fancy Entity Framework Core commands? (add, edit, delete)
            https://www.learnentityframeworkcore.com/dbset

            How can I do more advanced commands? (join, where, find)
            https://www.learnentityframeworkcore.com/dbset/querying-data

            I just want to write my own query, How can I do that?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            How can I know more about parameterized queries?
            https://docs.microsoft.com/en-us/ef/core/querying/raw-sql

            What is this "db" variable?
            https://www.learnentityframeworkcore.com/dbcontext


        */

        // GET: Tag
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            /*
            Alternate
            IEnumerable<Tag> tags = db.Tags.ToList();
            */

            string query = "select * from Tags";
            
            IEnumerable<Tag> tags = db.Tags.FromSql(query);
            
            return View(tags);
        }

        public ActionResult New()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Create(string TagName_New, string TagColor_New)
        {
            string query = "insert into tags (TagName, TagColor)" +
                " values (@name, @color)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@name", TagName_New);
            myparams[1] = new SqlParameter("@color", TagColor_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        //TODO: EDIT VIEW, EDIT, DELETE, SHOW

        public ActionResult Edit(int? id)
        {
            if ((id == null) || (db.Tags.Find(id) == null))
            {
                return NotFound();
            }
            string query = "select * from Tags where tagid=@id";
            SqlParameter param = new SqlParameter("@id",id);
            Tag mytag = db.Tags.FromSql(query, param).FirstOrDefault();
            return View(mytag);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string TagName, string TagColor)
        {
            if ((id == null) || (db.Tags.Find(id) == null)){
                return NotFound();
            }
            string query = "update tags set TagName=@name, TagColor=@color" +
                " where tagid=@id";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@name",TagName);
            myparams[1] = new SqlParameter("@color",TagColor);
            myparams[2] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }

        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if ((id==null)||(db.Tags.Find(id)==null))
            {
                return NotFound();

            }
            string query = "delete from tags where tagid=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query,param);
            return View("List");
        }

        public ActionResult Show(int? id)
        {
            if ((id == null) || (db.Tags.Find(id) == null))
            {
                return NotFound();

            }
            string query = "select * from Tags where tagid=@id";
            SqlParameter param = new SqlParameter("@id",id);

            Tag tagtoshow = db.Tags.Include(t=>t.pagesxtags).ThenInclude(pxt=>pxt.Page).SingleOrDefault(t=>t.TagID==id);

            return View(tagtoshow);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}