﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

//We are also using Entity Framework Core, so make sure you have these dependencies.
//https://learnentityframeworkcore.com
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using System.Threading.Tasks;

//also need model data
using http5204_blog_aspcore.Models;

//This is a Model. What we're doing is defining classes
//That will define our database and tables.

namespace http5204_blog_aspcore.Data
{

    //SubClass of IdentityDbContext
    //Question: Whats the difference between IdentityDbContext and DbContext?
    //https://forums.asp.net/t/2002296.aspx?DBContext+VS+IdentityDBContext
    //https://stackoverflow.com/questions/19902756/asp-net-identity-dbcontext-confusion
    //Answer: they are pretty much the same except IdentityDbContext has an extra users and roles dbset.
    public class BlogCMSContext : IdentityDbContext<ApplicationUser>
    {
        
        public BlogCMSContext(DbContextOptions<BlogCMSContext> options)
        : base(options)
        { 

        }
        

        public DbSet<Page> Pages { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Tag> Tags { get; set; }

        //Search result (Line 37 is the entire reason I switched this project from EF6 to EF Core)
        public DbQuery<SearchItem> SearchItems { get; set; }

        //Configuration for SqlServer done in ConfigureServices of Startup.cs

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Q: Christine, what the heck is all this?
            //A: It's called fluent API. It's a way of representing the relational data in a very explicit way.

            //Q: How can I know more about how this works?
            //A: https://www.learnentityframeworkcore.com/configuration/fluent-api

            //describing that a pagextag PK is composite of page and tag
            modelBuilder.Entity<PagexTag>()
                .HasKey(pxt => new { pxt.PageID, pxt.Tagid});

            //describing that pagextag is associated with one page
            //AND one page has many pagesxtags
            //AND pagesxtags has foreign key to pageid
            modelBuilder.Entity<PagexTag>()
                .HasOne(pxt => pxt.Page)
                .WithMany(pxt => pxt.pagesxtags)
                .HasForeignKey(pxt => pxt.PageID);

            //Describing that pagextag is associated to one tag
            //AND one tag has many pagesxtags
            //and pagesxtags has foreign key to tagid
            modelBuilder.Entity<PagexTag>()
                .HasOne(pxt => pxt.Tag)
                .WithMany(pxt => pxt.pagesxtags)
                .HasForeignKey(pxt => pxt.Tagid);

            //Author has many blogs, each blog has one author
            modelBuilder.Entity<Blog>()
                .HasOne(b=>b.Author)
                .WithMany(a=>a.Blogs)
                .HasForeignKey(b=>b.AuthorID);

            //blog has many pages, each page has one blog
            modelBuilder.Entity<Page>()
                .HasOne(p => p.Blog)
                .WithMany(b=>b.Pages)
                .HasForeignKey(p=>p.BlogID);

            //One author has one user and that user has one author
            modelBuilder.Entity<Author>()
                .HasOne(a => a.user)
                .WithOne(u => u.author)
                .HasForeignKey<ApplicationUser>(u => u.AuthorID);

            base.OnModelCreating(modelBuilder);
            //also need to specify that these models make tables
            modelBuilder.Entity<Blog>().ToTable("Blogs");
            modelBuilder.Entity<Page>().ToTable("Pages");
            modelBuilder.Entity<Tag>().ToTable("Tags");
            modelBuilder.Entity<Author>().ToTable("Authors");
            modelBuilder.Entity<PagexTag>().ToTable("PagesxTags");
   
            
        }
    }
}