﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace http5204blogaspcore.Migrations
{
    public partial class Childfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Authors_AuthorID",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_AuthorID",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<string>(
                name: "UserID",
                table: "Authors",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Authors_UserID",
                table: "Authors",
                column: "UserID",
                unique: true,
                filter: "[UserID] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Authors_AspNetUsers_UserID",
                table: "Authors",
                column: "UserID",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Authors_AspNetUsers_UserID",
                table: "Authors");

            migrationBuilder.DropIndex(
                name: "IX_Authors_UserID",
                table: "Authors");

            migrationBuilder.AlterColumn<string>(
                name: "UserID",
                table: "Authors",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_AuthorID",
                table: "AspNetUsers",
                column: "AuthorID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Authors_AuthorID",
                table: "AspNetUsers",
                column: "AuthorID",
                principalTable: "Authors",
                principalColumn: "AuthorID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
