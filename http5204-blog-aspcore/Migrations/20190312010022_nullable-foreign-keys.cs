﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace http5204blogaspcore.Migrations
{
    public partial class nullableforeignkeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Authors_AuthorID",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_AuthorID",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<int>(
                name: "AuthorID",
                table: "AspNetUsers",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_AuthorID",
                table: "AspNetUsers",
                column: "AuthorID",
                unique: true,
                filter: "[AuthorID] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Authors_AuthorID",
                table: "AspNetUsers",
                column: "AuthorID",
                principalTable: "Authors",
                principalColumn: "AuthorID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Authors_AuthorID",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_AuthorID",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<int>(
                name: "AuthorID",
                table: "AspNetUsers",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_AuthorID",
                table: "AspNetUsers",
                column: "AuthorID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Authors_AuthorID",
                table: "AspNetUsers",
                column: "AuthorID",
                principalTable: "Authors",
                principalColumn: "AuthorID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
