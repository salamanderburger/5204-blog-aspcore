﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_blog_aspcore.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        //Configure one to one relationship between user and author
        [ForeignKey("AuthorID")]
        public int? AuthorID { get; set; }

        //An Application User is tied to an author.
        public virtual Author author { get; set; }
    }
}
