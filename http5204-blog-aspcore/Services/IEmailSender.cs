﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace http5204_blog_aspcore.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
