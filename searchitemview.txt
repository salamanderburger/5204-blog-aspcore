/*
	instructions
	find your DB
	click "Views" folder
	new view
	paste the below text
	save file
	update database
	search feature should work
*/

CREATE VIEW SearchItems as
	select PageTitle as SearchTitle,
	PageContent as SearchContent,
	'Page' as SearchType,
	PageID as SearchID
	from Pages
	UNION
	select concat(AuthorFName, ' ', AuthorLName, ' a.k.a ', AuthorPenName) as SearchTitle,
	'' as SearchContent,
	'Author' as SearchType,
	AuthorID as SearchID from Authors
	UNION
	select BlogTitle as SearchTitle,
	BlogBio as SearchContent,
	'Blog' as SearchType,
	BlogID as SearchID
	from Blogs
	UNION
	select TagName as SearchTitle,
	'' as SearchContent,
	'Tag' as SearchType,
	TagID as TagID
	from Tags